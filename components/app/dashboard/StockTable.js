import { Table, Button, Accordion, Title } from "@mantine/core";
import { cx, css } from "@emotion/css";

export default function StockTable({ user }) {
  return (
    <div>
      <Title
        order={4}
        className={cx(
          css({
            padding: "0.5rem",
          })
        )}
      >
        Stocks
      </Title>
      <Table>
        <thead>
          <tr>
            <th>Ticker</th>
            <th>Amount</th>
            <th>Total Deposit</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {user.stocks.map((stock, key) => {
            return (
              <tr key={key}>
                <td>{stock.ticker}</td>
                <td>{stock.amount}</td>
                <td>${stock.avgPrice * stock.amount}</td>
                <td>
                  <Button variant="light">Buy/Sell</Button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    </div>
  );
}
