import React, { useRef, useEffect } from "react";
import { init, getInstanceByDom } from "echarts";

function convertUserData(user) {
  var data = [{}];
  user.stocks.forEach((stock) => {
    data.push({
      name: stock.name,
      value: stock.amount * stock.avgPrice,
    });
  });
  user.indexes.forEach((index) => {
    var index_item = { name: index.name, children: [] };
    index.index.forEach((index_stock) => {
      index_item.children.push({
        name: index_stock.name,
        value: index_stock.amount * index_stock.avgPrice,
      });
    });
    data.push(index_item);
  });
  return data;
}

export default function PieChart(props) {
  var data = convertUserData(props.user);
  var option = {
    tooltip: {
      trigger: "item",
    },
    series: {
      type: "sunburst",
      data: data,
      avoidLabelOverlap: false,
      radius: [0, "100%"],
      label: {
        show: false,
      },

      labelLine: {
        show: false,
      },
      levels: [
        {},
        {
          r0: "40%",
          r: "75%",
          itemStyle: {
            borderWidth: 2,
          },
          label: {
            rotate: "radial",
          },
        },
        {
          r0: "75%",
          r: "99%",
          itemStyle: {
            borderWidth: 2,
          },
        },
      ],
    },
    grid: {
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
    },
  };
  const chartRef = useRef(null);
  useEffect(() => {
    let chart;
    if (chartRef.current !== null) {
      chart = init(chartRef.current);
    }
    function resizeChart() {
      chart?.resize();
    }
    window.addEventListener("resize", resizeChart);
    return () => {
      chart?.dispose();
      window.removeEventListener("resize", resizeChart);
    };
  }, []);

  useEffect(() => {
    if (chartRef.current !== null) {
      const chart = getInstanceByDom(chartRef.current);
      chart.setOption(option);
    }
  }, []);

  return <div ref={chartRef} style={{ width: "100%", height: "50vh" }} />;
}
