import UpDown from "../UpDown";
import { Table } from "@mantine/core";
import { cx, css } from "@emotion/css";

export default function MonetaryData({ user }) {
  const temp = user.stocks.reduce(
    (previousValue, currentValue) =>
      previousValue + currentValue.amount * currentValue.avgPrice,
    0
  );
  const sum = user.indexes.reduce((previous, index) => {
    const indexValue = index.index.reduce(
      (previousValue, currentValue) =>
        previousValue + currentValue.amount * currentValue.avgPrice,
      0
    );
    return previous + indexValue;
  }, temp);
  return (
    <Table>
      <thead>
        <tr>
          <th>Total Deposits</th>
          <th>Cash Available</th>
          <th>Total Assets</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>$ {sum}</td>
          <td>$ {user.cashAvailable}</td>
          <td>$ {sum + user.cashAvailable}</td>
        </tr>
      </tbody>
    </Table>
  );
}
