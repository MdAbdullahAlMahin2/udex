import { Button, Accordion, Table, Text, Badge, Title } from "@mantine/core";
import { cx, css } from "@emotion/css";

export default function StockTable({ user }) {
  return (
    <div>
      <Title
        order={4}
        className={cx(
          css({
            padding: "1rem",
          })
        )}
      >
        Indexes
      </Title>
      <Accordion chevronSize={40} variant="filled">
        {user.indexes.map((index, key) => {
          const sum = index.index.reduce((previous, stock) => {
            return previous + stock.avgPrice * stock.amount;
          }, 0);
          return (
            <Accordion.Item value={index.name} key={key}>
              <Accordion.Control>{index.name}</Accordion.Control>
              <Accordion.Panel>
                <div
                  className={cx(
                    css({
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "space-between",
                    })
                  )}
                >
                  <Text>
                    Total Value{" "}
                    <Badge size="lg" radius="lg">
                      ${sum}
                    </Badge>
                  </Text>
                  <div
                    className={cx(
                      css({
                        display: "flex",
                      })
                    )}
                  >
                    <Button variant="light">Buy/Sell</Button>
                    <div
                      className={cx(
                        css({
                          height: "0.5rem",
                          width: "0.5rem",
                        })
                      )}
                    />
                    <Button variant="light">Re-Balance</Button>
                  </div>
                </div>
                <br />
                <Table>
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Ticker</th>
                      <th>Total Deposited</th>
                      <th>Proportion</th>
                    </tr>
                  </thead>
                  <tbody>
                    {index.index.map((stock, key) => {
                      return (
                        <tr key={key}>
                          <td>{stock.name}</td>
                          <td>{stock.ticker}</td>
                          <td>${stock.avgPrice * stock.amount}</td>
                          <td>{stock.proportion * 100}%</td>
                        </tr>
                      );
                    })}
                  </tbody>
                </Table>
              </Accordion.Panel>
            </Accordion.Item>
          );
        })}
      </Accordion>
    </div>
  );
}
