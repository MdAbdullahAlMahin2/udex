import {
  AppShell,
  Navbar,
  Header,
  Footer,
  MediaQuery,
  Text,
  Title,
  Button,
  Menu,
  Avatar,
  ActionIcon,
  HoverCard,
  Tooltip,
} from "@mantine/core";
import { useState } from "react";
import { cx, css } from "@emotion/css";
import {
  IconHome,
  IconSearch,
  IconChartDonut3,
  IconSettings,
  IconUserCircle,
  IconLogout,
} from "@tabler/icons";
import { useUser } from "@auth0/nextjs-auth0";
import { useRouter } from "next/router";
import Link from "next/link";

export default function AppContainer({ children }) {
  const { user, error, isLoading } = useUser();
  if (isLoading) return <div>Loading...</div>;
  if (error) return <div>{error.message}</div>;
  if (typeof user === "undefined") {
    return <div>Log In First</div>;
  }
  return (
    user && (
      <AppShell
        navbar={
          <MediaQuery smallerThan={600} styles={{ display: "none" }}>
            <Navbar width={{ base: 70 }} p="sm">
              {childrenNavbar()}
            </Navbar>
          </MediaQuery>
        }
        navbarOffsetBreakpoint={600}
        header={
          <Header height={65} p="xs">
            {childrenHeader()}
          </Header>
        }
        footer={
          <div>
            <MediaQuery largerThan={600} styles={{ display: "none" }}>
              <Footer height={60} p="xs">
                {childrenFooter()}
              </Footer>
            </MediaQuery>
            <MediaQuery smallerThan={600} styles={{ display: "none" }}>
              <Footer height={0}></Footer>
            </MediaQuery>
          </div>
        }
        styles={(theme) => ({
          main: {
            backgroundColor:
              theme.colorScheme === "dark"
                ? theme.colors.dark[8]
                : theme.colors.gray[1],
          },
        })}
      >
        {children}
      </AppShell>
    )
  );
}

// className={cx(
//         css({
//           padding: "0.75rem",
//           width: "100%",
//           length: "100%",
//           fontSize: "1.25rem",
//         })
//       )}

function childrenHeader() {
  return (
    <div
      className={cx(
        css({
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          padding: "0 0.5rem",
        })
      )}
    >
      <Title order={1}>Title (uDex)</Title>
      <Menu shadow="md" width={125}>
        <Menu.Target>
          <Avatar />
        </Menu.Target>
        <Menu.Dropdown>
          <Menu.Item icon={<IconSettings size={16} />}>Settings</Menu.Item>
          <Menu.Item icon={<IconUserCircle size={16} />}>Profile</Menu.Item>
          <Menu.Divider />
          <Menu.Item icon={<IconLogout size={16} />} color="red">
            <Link href="/api/auth/logout">
              <a>Log Out</a>
            </Link>
          </Menu.Item>
        </Menu.Dropdown>
      </Menu>
    </div>
  );
}

function childrenNavbar() {
  return (
    <div
      className={cx(
        css({
          height: "35%",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "space-between",
        })
      )}
    >
      <Tooltip label="Dashboard" position="bottom">
        <ActionIcon size="xl">
          <IconHome size={32} />
        </ActionIcon>
      </Tooltip>
      <Tooltip label="Explore Stocks" position="bottom">
        <ActionIcon size="xl">
          <IconSearch size={32} />
        </ActionIcon>
      </Tooltip>
      <Tooltip label="Indexes" position="bottom">
        <ActionIcon size="xl">
          <IconChartDonut3 size={32} />
        </ActionIcon>
      </Tooltip>
    </div>
  );
}

function childrenFooter() {
  return (
    <div
      className={cx(
        css({
          display: "flex",
          alignItems: "center",
          justifyContent: "space-around",
        })
      )}
    >
      <Tooltip label="Dashboard" position="bottom">
        <ActionIcon size="xl">
          <IconHome size={32} />
        </ActionIcon>
      </Tooltip>
      <Tooltip label="Explore Stocks" position="bottom">
        <ActionIcon size="xl">
          <IconSearch size={32} />
        </ActionIcon>
      </Tooltip>
      <Tooltip label="Indexes" position="bottom">
        <ActionIcon size="xl">
          <IconChartDonut3 size={32} />
        </ActionIcon>
      </Tooltip>
    </div>
  );
}
