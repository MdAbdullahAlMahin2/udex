import { IconChevronsUp, IconChevronsDown } from "@tabler/icons";
import { cx, css } from "@emotion/css";

export default function UpDown({ number }) {
  return (
    <div>
      {number > 0 ? (
        <div
          className={cx(
            css({
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              color: "#12B886",
            })
          )}
        >
          <IconChevronsUp /> {`${number}% `}
        </div>
      ) : (
        <div
          className={cx(
            css({
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              color: "#FA5252",
            })
          )}
        >
          <IconChevronsDown /> {`${number}%`}
        </div>
      )}
    </div>
  );
}
