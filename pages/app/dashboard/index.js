import { Card, Box } from "@mantine/core";
import { cx, css } from "@emotion/css";
import { AppContainer } from "../../../components/app";
import {
  PieChart,
  MonetaryData,
  StockTable,
  IndexTable,
} from "../../../components/app/dashboard";
import { useState, useEffect } from "react";

export default function Dashboard() {
  var user = {
    cashAvailable: 0,
    stocks: [
      // {
      //   name: "Apple",
      //   amount: 50,
      //   ticker: "APPL",
      //   avgPrice: 100,
      // },
      // {
      //   name: "Tesla",
      //   amount: 100,
      //   ticker: "TSLA",
      //   avgPrice: 65,
      // },
      // {
      //   name: "Alphabet",
      //   amount: 150,
      //   ticker: "GOOGL",
      //   avgPrice: 85,
      // },
    ],
    indexes: [
      // {
      //   name: "My First Index",
      //   index: [
      //     {
      //       name: "Alphabet",
      //       proportion: 0.7,
      //       amount: 150,
      //       ticker: "GOOGL",
      //       avgPrice: 85,
      //     },
      //     {
      //       name: "Tesla",
      //       proportion: 0.3,
      //       amount: 84,
      //       ticker: "TSLA",
      //       avgPrice: 65,
      //     },
      //   ],
      // },
    ],
    watchList: ["AAPL"],
  };
  const breakpoints = [780, 600, 460];
  const mq = breakpoints.map((bp) => `@media (max-width: ${bp}px)`);
  useEffect(() => {
    fetch("https://fast-academy-357605.et.r.appspot.com/portfolio/getstocks", {
      method: "GET",
    })
      .then((response) => response.json())
      .then((data) => console.log(data));
    // fetch("https://fast-academy-357605.et.r.appspot.com/portfolio/getindexes", {
    //   method: "GET",
    // }).then((res) => {
    //   console.log("Indexes: ", res);
    // });
    // fetch(
    //   "https://fast-academy-357605.et.r.appspot.com/portfolio/getcashbalance",
    //   {
    //     method: "GET",
    //   }
    // ).then((res) => {
    //   console.log("Cashbalance: ", res);
    // });
  });
  return (
    <AppContainer>
      <div
        className={css({
          display: "flex",
          width: "100%",
          [mq[0]]: {
            flexDirection: "column",
          },
        })}
      >
        <div
          className={cx(
            css({
              width: "35%",
              [mq[0]]: {
                width: "100%",
                display: "flex",
                alignItems: "center",
                justifyContent: "space-around",
              },
              [mq[1]]: {
                flexDirection: "column",
                alignItems: "normal",
              },
            })
          )}
        >
          <Card
            p="xs"
            radius="sm"
            className={cx(
              css({
                [mq[0]]: {
                  width: "45%",
                },
                [mq[1]]: {
                  width: "100%",
                },
              })
            )}
            withBorder
          >
            <PieChart user={user} />
          </Card>
          <div
            className={cx(
              css({
                height: "0.85rem",
                [mq[0]]: {
                  height: "100%",
                  width: "0.75rem",
                },
                [mq[1]]: {
                  height: "0.75rem",
                },
              })
            )}
          />
          <Card p="xs" radius="sm" withBorder>
            <MonetaryData user={user} />
          </Card>
        </div>
        <div
          className={cx(
            css({
              width: "0.75rem",
              [mq[0]]: {
                width: "100%",
                height: "0.75rem",
              },
            })
          )}
        />
        <div
          className={cx(
            css({
              width: "65%",
              [mq[0]]: {
                width: "100%",
                display: "flex",
                flexDirection: "column",
              },
            })
          )}
        >
          <Card p="xs" radius="sm" withBorder>
            <StockTable user={user} />
          </Card>
          <div
            className={cx(
              css({
                height: "0.85rem",
              })
            )}
          />
          <Card p="0" radius="sm" withBorder>
            <IndexTable user={user} />
          </Card>
          <div
            className={cx(
              css({
                width: "65%",
                [mq[1]]: {
                  height: "4rem",
                },
              })
            )}
          />
        </div>
      </div>
    </AppContainer>
  );
}
